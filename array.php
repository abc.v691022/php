<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- http://www.w3schools.com/php/php_ref_array.asp
  An array stores multiple values in one single variable

  Array Operators
    +    Union of $x and $y
    ==   Returns true if $x and $y have the same key/value pairs
    ===  Returns true if $x and $y have the same key/value pairs in the same order and of the same types
    !=   Returns true if $x is not equal to $y
    <>   Returns true if $x is not equal to $y
    !==  Returns true if $x is not identical to $y

  Types of arrays: 3
    Indexed arrays - Arrays with a numeric index
    Associative arrays - Arrays with named keys
    Multidimensional arrays - Arrays containing one or more arrays
  
  count() function is used to return the length (the number of elements) of an array

  sort() - sort arrays in ascending order
  rsort() - sort arrays in descending order
  asort() - sort associative arrays in ascending order, according to the value
  ksort() - sort associative arrays in ascending order, according to the key
  arsort() - sort associative arrays in descending order, according to the value
  krsort() - sort associative arrays in descending order, according to the key
-->
<?php
  // Indexed Array
  $cars = array("Volvo", "BMW", "Toyota");
  $arrlength = count($cars);
  for($x = 0; $x < $arrlength; $x++) {
    echo $cars[$x];
    echo "<br>";
  }

  // Associative Array
  $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
  foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
  }

  // two-dimensional array
  $cars = array (
    array("Volvo",22,18),
    array("BMW",15,13),
    array("Saab",5,2),
    array("Land Rover",17,15)
  );
  for ($row = 0; $row < 4; $row++) {
    echo "<ul>";
    for ($col = 0; $col < 3; $col++) {
      echo "<li>".$cars[$row][$col]."</li>";
    }
    echo "</ul>";
  }
?>  
</body>
</html>