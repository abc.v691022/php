<?php
  $cookie_name = "user";
  $cookie_value = "John Doe";
  setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
  
  // set the expiration date to one hour ago
  // setcookie("user", "", time() - 3600);
?>
<?php
  // Start the session
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- Cookie 
  A cookie is often used to identify a user. 
  A cookie is a small file that the server embeds on the user's computer. 
  Each time the same computer requests a page with a browser, it will send the cookie too. 
  The value of the cookie is automatically URLencoded when sending the cookie, and automatically decoded when received (to prevent URLencoding, use setrawcookie() instead).
  
  setcookie(name, [value, expire, path, domain, secure, httponly]); must appear BEFORE the <html> tag
  To delete a cookie, use the setcookie() function with an expiration date in the past

SESSIONS
  A session is a way to store information (in variables) to be used across multiple pages.
  By default, session variables last until the user closes the browser.

  session_start() function must be the very first thing in your document. Before any HTML tags.
  session_unset()
  session_destroy()
-->

<?php
  if(!isset($_COOKIE[$cookie_name])) {
      echo "Cookie named '" . $cookie_name . "' is not set!";
  } else {
      echo "Cookie '" . $cookie_name . "' is set!<br>";
      echo "Value is: " . $_COOKIE[$cookie_name];
  }
  if(count($_COOKIE) > 0) {
      echo "Cookies are enabled.";
  } else {
      echo "Cookies are disabled.";
  }
?>

<?php
  // Set session variables
  $_SESSION["favcolor"] = "green";
  $_SESSION["favanimal"] = "cat";
  echo "Favorite color is " . $_SESSION["favcolor"] . ".<br>";
  print_r($_SESSION); // show all the session variable values

  // remove all session variables
  session_unset(); 
  // destroy the session 
  session_destroy();
?>
</body>
</html>