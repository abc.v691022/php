<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- http://www.w3schools.com/php/php_ref_string.asp
  strlen(): returns the length of a string 
  str_word_count(): counts the number of words in a string
  strrev() function reverses a string
  strpos() function searches for a specific text within a string
    returns the character position of the first match. If no match is found, it will return FALSE.
  str_replace() function replaces some characters with some other characters in a string
  join([separator],array) returns a string from the elements of an array
    is an alias of the implode() function. 
-->
<?php
  $arr = array('Hello','World!','Beautiful','Day!');

  echo strlen("Hello world!"); // outputs 12
  echo str_word_count("Hello world!"); // outputs 2
  echo strrev("Hello world!"); // outputs !dlrow olleH
  echo strpos("Hello world!", "world"); // outputs 6
echo str_replace("world", "Dolly", "Hello world!"); // outputs Hello Dolly!
echo join(" ",$arr)."<br>"; // Hello World! Beautiful Day!

?>
</body>
</html>