<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- GET vs. POST
  Both create an array which holds key/value pairs, where keys are the names of the form controls and values are the input data from the user.
  GET method: 
    because all variable names and values are displayed in the URL, it is possible to bookmark the page 
    has limits on the amount of information to send (2000 characters). 
    be used for sending non-sensitive data.
  POST method:
    all names/values are embedded within the body of the HTTP request
    has no limits on the amount of information to send.
    supports advanced functionality such as support for multi-part binary input while uploading files to server

  $_SERVER["PHP_SELF"] is a super global variable that returns the filename of the currently executing script.
  htmlspecialchars(): converts special characters to HTML entities.
  $_SERVER["REQUEST_METHOD"]: check whether the form has been submitted
  trim(): Strip unnecessary characters (extra space, tab, newline) from the user input data
  stripslashes(): Remove backslashes (\) from the user input data
  empty(): checks if the $_POST variable is empty
  preg_match(): searches a string for pattern, returning true if the pattern exists, and false otherwise.
  
  http://www.w3schools.com/php/php_ref_filter.asp
  filter_list(): list what the PHP filter extension offers
  filter_var($variable, typeofcheck): check whether an email address is well-formed
    FILTER_SANITIZE_STRING: remove all HTML tags from a string
    FILTER_VALIDATE_INT: check if the variable is an integer. Problem: if variable = 0, return false
      if (filter_var($int, FILTER_VALIDATE_INT) === 0 || !filter_var($int, FILTER_VALIDATE_INT) === false)

    FILTER_VALIDATE_IP
    FILTER_SANITIZE_EMAIL: remove all illegal characters from variable
    FILTER_VALIDATE_EMAIL: check if it is a valid email address
    FILTER_SANITIZE_URL
    FILTER_VALIDATE_URL
-->
<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
  }
    
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field.</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
  Gender:
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
?>


<table>
  <tr>
    <td>Filter Name</td>
    <td>Filter ID</td>
  </tr>
  <?php
  foreach (filter_list() as $id =>$filter) {
      echo '<tr><td>' . $filter . '</td><td>' . filter_id($filter) . '</td></tr>';
  }
  ?>
</table>
<?php
  $int = 122;
  $min = 1;
  $max = 200;

  if (filter_var($int, FILTER_VALIDATE_INT, array("options" => array("min_range"=>$min, "max_range"=>$max))) === false) {
      echo("Variable value is not within the legal range");
  } else {
      echo("Variable value is within the legal range");
  }
?>
<?php
  $url = "http://www.w3schools.com";

  if (!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_QUERY_REQUIRED) === false) {
      echo("$url is a valid URL");
  } else {
      echo("$url is not a valid URL");
  }
?>
<?php
  $str = "<h1>Hello WorldÆØÅ!</h1>";
  // Remove Characters With ASCII Value > 127
  $newstr = filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  echo $newstr;
?>
</body>
</html>