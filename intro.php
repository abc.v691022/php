<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- PHP(Hypertext Preprocessor)
  PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages. 
  PHP is a Loosely Typed Language. PHP automatically converts the variable to the correct data type, depending on its value.

What is a PHP File?
  PHP files can contain text, HTML, CSS, JavaScript, and PHP code
  PHP code are executed on the server, and the result is returned to the browser as plain HTML
  PHP files have extension ".php"

Why PHP?
  PHP runs on various platforms (Windows, Linux, Unix, Mac OS X, etc.)
  PHP is compatible with almost all servers used today (Apache, IIS, etc.)
  PHP supports a wide range of databases
  PHP is free. Download it from the official PHP resource: www.php.net
  PHP is easy to learn and runs efficiently on the server side

Installation
  Find a web host with PHP and MySQL support
  Install a web server on your own PC, and then install PHP and MySQL

Comments
  // This is a single-line comment
  # This is also a single-line comment
  /*
  This is a multiple-lines comment block
  */
Data Types
  String
  Integer
  Float (floating point numbers - also called double)
  Boolean
  Array
  Object
  NULL: If a variable is created without a value, it is automatically assigned a value of NULL
  Resource

Case Sensitivity
  all keywords (e.g. if, else, while, echo, etc.), classes, functions, and user-defined functions are NOT case-sensitive
  all variable names are case-sensitive.

Syntax
  A PHP script can be placed anywhere in the document.

VARIABLES
  Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it.
  A variable starts with the $ sign, followed by the name of the variable
  A variable name must start with a letter or the underscore character
  A variable name cannot start with a number
  A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
  Variable names are case-sensitive ($age and $AGE are two different variables)
Variable scopes
  global: A variable declared outside a function has a GLOBAL SCOPE and can only be accessed outside a function
  local: A variable declared within a function has a LOCAL SCOPE and can only be accessed within that function:
  static

  "GLOBAL" Keyword: is used to access a global variable from within a function.
  "STATIC" Keyword: when a function is completed/executed, all of its variables are deleted. However, sometimes we want a local variable NOT to be deleted. Then, each time the function is called, that variable will still have the information it contained from the last time the function was called. The variable is still local to the function.
Constants
  syntax: define(name, value, case-insensitive);
  Constants are like variables except that once they are defined they cannot be changed or undefined.
  starts with a letter or underscore (no $ sign before the constant name)
  Unlike variables, constants are automatically global across the entire script.

OUTPUT:  two basic ways to get output: echo and print
  echo has no return value while print has a return value of 1 so it can be used in expressions. 
  echo can take multiple parameters (although such usage is rare) while print can take one argument. 
  echo is marginally faster than print.
ECHO Statement
  can be used with or without parentheses: echo or echo()

Operators
  Arithmetic operators: + - * / %
  Assignment operators:*=  = += -= /= %=
  Comparison operators: ==, ===, <>(not equal), >, >=, <, <=, !=, !==
  Increment/Decrement operators:++$x, $x++, --$x, $x--
  Logical operators: and, or, xor(True if either $x or $y is true, but not both), &&, ||, !
  String operators: .(Concatenation), .=
  Array operators: +, ==, ===, !=, <>, !==
-->
<?php
  $x = 5;
  $y = 10;
  define("GREETING", "Welcome to W3Schools.com!", true);

  function myTest() {
    global $x, $y;
    $y = $x + $y;
  }
  function myTest01() {
    $GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
  } 
  function myTest() {
    static $x = 0;
    echo $x;
    $x++;
  }

  myTest();
  echo $y; // outputs 15
  echo "<h2>$txt1</h2>";
  echo "This ", "string ", "was ", "made ", "with multiple parameters.";
?>

<?php 
  $x = 10.365;
  $cars = array("Volvo","BMW","Toyota");
  var_dump($x); // float(10.365)
  // var_dump() function returns the data type and value

  class Car {
    function Car() {
        $this->model = "VW";
    }
  }
  // create an object
  $herbie = new Car();
  // show object properties
  echo $herbie->model;
?>
</body>
</html>