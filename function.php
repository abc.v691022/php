<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- FUNCTION
  A function is a block of statements that can be used repeatedly in a program.
  A function will not execute immediately when a page loads.
  A function will be executed by a call to the function.
  A function name can start with a letter or underscore (not a number)
  Function names are NOT case-sensitive.

Arguments
  add as many arguments as you want, just separate them with a comma.
-->
<?php
function setHeight($minheight = 50) {
    echo "The height is : $minheight <br>";
}
setHeight(); // will use the default value of 50
?>  
</body>
</html>