<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<!-- 
Conditional Statements
  if statement - executes some code if one condition is true
  if...else statement - executes some code if a condition is true and another code if that condition is false
  if...elseif....else statement - executes different codes for more than two conditions
  switch statement - selects one of many blocks of code to be executed

Loops
  while - loops through a block of code as long as the specified condition is true
  do...while - loops through a block of code once, and then repeats the loop as long as the specified condition is true
  for - loops through a block of code a specified number of times
  foreach - loops through a block of code for each element in an array
-->
<?php
  $t = date("H");
  if ($t < "10") {
      echo "Have a good morning!";
    } elseif ($t < "20") {
      echo "Have a good day!";
    } else {
      echo "Have a good night!";
  }

  $favcolor = "red";
  switch ($favcolor) {
    case "red":
      echo "Your favorite color is red!";
      break;
    case "blue":
      echo "Your favorite color is blue!";
      break;
    case "green":
      echo "Your favorite color is green!";
      break;
    default:
      echo "Your favorite color is neither red, blue, nor green!";
  }

  $x = 1; 
  do {
    echo "The number is: $x <br>";
    $x++;
  } while ($x <= 5);

  for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
  } 

  $colors = array("red", "green", "blue", "yellow"); 
  foreach ($colors as $value) {
      echo "$value <br>";
  }
?>

<!-- http://www.w3schools.com/php/php_superglobals.asp 
Global Variables - Superglobals(means that they are always accessible, regardless of scope)
  $GLOBALS[index]: PHP also stores all global variables in an array. The index holds the name of the variable.
  $_SERVER: holds information about headers, paths, and script locations
  $_REQUEST: collect data after submitting an HTML form
  $_POST: collect form data after submitting an HTML form with method="post"
  $_GET
  $_FILES
  $_ENV
  $_COOKIE
  $_SESSION 
-->

<!-- Date 
  date(format,timestamp) return the current date/time of the server
  d - Represents the day of the month (01 to 31)
  m - Represents a month (01 to 12)
  Y - Represents a year (in four digits)
  l (lowercase 'L') - Represents the day of the week
  
  h - 12-hour format of an hour with leading zeros (01 to 12)
  i - Minutes with leading zeros (00 to 59)
  s - Seconds with leading zeros (00 to 59)
  a - Lowercase Ante meridiem and Post meridiem (am or pm)
  "/", ".", or "-" can also be inserted between the characters to add additional formatting.

  date_default_timezone_set();
  mktime(hour,minute,second,month,day,year);
  strtotime(time, now) create a date from a string
-->
<?php
  date_default_timezone_set("America/New_York");
  $d=strtotime("10:30pm April 15 2014");  // 1397615400
  echo "Today is " . date("Y/m/d") . "<br>"; // Today is 2017/02/10
  echo "The time is " . date("h:i:sa");
  echo "Created date is " . date("Y-m-d h:i:sa", $d);

  function nextSixSaturday(){
    $startdate = strtotime("Saturday");
    $enddate = strtotime("+6 weeks", $startdate);

    while ($startdate < $enddate) {
      echo date("M d", $startdate) . "<br>";
      $startdate = strtotime("+1 week", $startdate);
    }
  }

  function until4July(){
    $d1=strtotime("July 04");
    $d2=ceil(($d1-time())/60/60/24);
    echo "There are " . $d2 ." days until 4th of July.";
  }
?>

<!-- include Statements
  usage: include the same PHP, HTML, or text on multiple pages of a website. 
  require will produce a fatal error (E_COMPILE_ERROR) and stop the script
  include will only produce a warning (E_WARNING) and the script will continue

  if you want the execution to go on and show users the output, even if the include file is missing, use the include statement. 
  in case of FrameWork, CMS, or a complex PHP application coding, always use the require statement to include a key file to the flow of execution. This will help avoid compromising your application's security and integrity, just in-case one key file is accidentally missing.
-->
<?php include 'footer.php';?>

<!-- Manipulating Files 
http://www.w3schools.com/php/php_ref_filesystem.asp

file_exists()
readfile(): only open up a file and read its contents
fopen(file-name, option): open or create a file if it does not exist
  r Open a file for read only. File pointer starts at the beginning of the file
  w Open a file for write only. Erases the contents of the file or creates a new file if it doesn't exist. File pointer starts at the beginning of the file
  a Open a file for write only. The existing data in file is preserved. File pointer starts at the end of the file. Creates a new file if the file doesn't exist
  x Creates a new file for write only. Returns FALSE and an error if file already exists
  r+  Open a file for read/write. File pointer starts at the beginning of the file
  w+  Open a file for read/write. Erases the contents of the file or creates a new file if it doesn't exist. File pointer starts at the beginning of the file
  a+  Open a file for read/write. The existing data in file is preserved. File pointer starts at the end of the file. Creates a new file if the file doesn't exist
  x+  Creates a new file for read/write. Returns FALSE and an error if file already exists

fgets(): read a single line from a file
fgetc(): read a single character from a file
fread()
fwrite()
fclose()
feof(): checks if the "end-of-file" has been reached(for looping through data of unknown length)

File Upload
  "php.ini" file: file_uploads = On
  <form enctype="multipart/form-data">: specifies which content-type to use when submitting the form
-->
<?php
  if(!file_exists("welcome.txt")) {
    die("File not found");
  } else {
    $myfile = fopen("webdictionary.txt", "r") or die("Unable to open file!");
    echo fread($myfile,filesize("webdictionary.txt"));
    // while(!feof($myfile)) {
    //   echo fgets($myfile) . "<br>";
    // }
    fclose($myfile);
  }
  
?>

<!-- upload.php -->
<?php
  $target_dir = "uploads/";
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
  $uploadOk = 1;
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  // Check if image file is a actual image or fake image
  if(isset($_POST["submit"])) {
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
          echo "File is an image - " . $check["mime"] . ".";
          $uploadOk = 1;
      } else {
          echo "File is not an image.";
          $uploadOk = 0;
      }
  }
  // Check if file already exists
  if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
  }
  // Check file size
  if ($_FILES["fileToUpload"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
  }
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif" ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
  // if everything is ok, try to upload file
  } else {
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
          echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
      } else {
          echo "Sorry, there was an error uploading your file.";
      }
  }
?>
<form action="upload.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>


</body>
</html>